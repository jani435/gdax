gdax app

This django app can be used to store gdax order book in mongodb and calculate mid market price based on order data.
This app is based on websockets and eveloped using library [gdax-api](https://github.com/danpaquin/gdax-python)

Currently it is using [pymongo](https://api.mongodb.com/python/current/) as a database driver for mongodb but we can use mongoengine or djanogo in future.

