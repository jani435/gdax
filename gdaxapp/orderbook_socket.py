# -*- coding: utf-8 -*-
import gdax
import pymongo
import copy
from pymongo import MongoClient
from bson.decimal128 import Decimal128
from bintrees import RBTree
mongo_client = MongoClient('mongodb://127.0.0.1:27017/')

# specify the database and collection
db = mongo_client.test
order_collection = db.order_collection
bid_collection = db.bid_collection
ask_collection = db.ask_collection

class OrderBookSocket(gdax.OrderBook):

    def __init__(self, product_id=None):
        super(OrderBookSocket, self).__init__(product_id=product_id)
        # latest values of bid-ask spread
        self._bid = None
        self._ask = None
        self._started = False

    def remove_asks(self, price):
        super(OrderBookSocket, self).remove_asks(price)
        ask_collection.remove({'price': price})

    def set_asks(self, price, asks):
        """overrided set_asks function to store asks record in db"""
        super(OrderBookSocket, self).set_asks(price, asks)
        for ask in asks:
            if ask.has_key('_id'):
                pass
            else:
                ask_collection.insert_one(ask)

    def remove_bids(self, price):
        super(OrderBookSocket, self).remove_bids(price)
        bid_collection.remove({'price': price})

    def set_bids(self, price, bids):
        """overrided set_bids function to store bids record in db"""
        super(OrderBookSocket, self).set_bids(price, bids)
        for bid in bids:
            if bid.has_key('_id'):
               pass
            else: 
                bid_collection.insert_one(bid)
    
    def on_message(self, message):
        super(OrderBookSocket, self).on_message(message)
        #uncomment if want to save every order in collection
        # self.save_order(message)

    def get_mm_price_db(self):
        """get mid market price from datbase using asks and bids"""
        buy = [b for b in bid_collection.find().sort([("price", pymongo.DESCENDING)]).limit(1)]
        sell = [s for s in ask_collection.find().sort([("price", pymongo.ASCENDING)]).limit(1)]
        if buy and sell:
            avg = (float(buy[0]['price'])+float(sell[0]['price']))/2
            return avg
        else:
            return None
             
    def get_mm_price_api(self):
        """get mm price from the api, not stored in database"""
        if self._bids and self._asks:
            bid = self.get_bid()
            ask = self.get_ask()
            if bid and ask:
                return float(0.5*(float(bid)+float(ask)))
            else:
                return None
        else:
            return None

    def save_order(self, message):
        """Used to store every incoming order in order's collection, 
            if required can be invoked in on_message() """
        if 'price' in message and 'type' in message:
            if order_collection:
                order_collection.insert_one(message)

    def on_close(self):
        super(OrderBookSocket, self).on_close()
        self._started = False

    def set_started(self):
        self._started = True

    def get_started(self):
        return self._started

    """All the following five methods are overrided to use float value instead of decomal 
        due to an issue in mongodb while storing decimals
        see: http://api.mongodb.com/python/current/faq.html#how-can-i-store-decimal-decimal-instances"""
    def reset_book(self):
        self._asks = RBTree()
        self._bids = RBTree()
        res = self._client.get_product_order_book(product_id=self.product_id, level=3)
        for bid in res['bids']:
            self.add({
                'id': bid[2],
                'side': 'buy',
                'price': float(bid[0]),
                'size': float(bid[1])
            })
        for ask in res['asks']:
            self.add({
                'id': ask[2],
                'side': 'sell',
                'price': float(ask[0]),
                'size': float(ask[1])
            })
        self._sequence = res['sequence']
        if ask_collection:
            ask_collection.remove({})
        if bid_collection:
            bid_collection.remove({})

    def add(self, order):
        order = {
            'id': order.get('order_id') or order['id'],
            'side': order['side'],
            'price': float(order['price']),
            'size': float(order.get('size') or order['remaining_size'])
        }
        if order['side'] == 'buy':
            bids = self.get_bids(order['price'])
            if bids is None:
                bids = [order]
            else:
                bids.append(order)
            self.set_bids(order['price'], bids)
        else:
            asks = self.get_asks(order['price'])
            if asks is None:
                asks = [order]
            else:
                asks.append(order)
            self.set_asks(order['price'], asks)

    def remove(self, order):
        # price = order['price']
        price = float(order['price'])
        if order['side'] == 'buy':
            bids = self.get_bids(price)
            if bids is not None:
                bids = [o for o in bids if o['id'] != order['order_id']]
                if len(bids) > 0:
                    self.set_bids(price, bids)
                else:
                    self.remove_bids(price)
        else:
            asks = self.get_asks(price)
            if asks is not None:
                asks = [o for o in asks if o['id'] != order['order_id']]
                if len(asks) > 0:
                    self.set_asks(price, asks)
                else:
                    self.remove_asks(price)

    def match(self, order):
        size = float(order['size'])
        price = float(order['price'])
        # size = order['size']
        # price = order['price']
        if order['side'] == 'buy':
            bids = self.get_bids(price)
            if not bids:
                return
            assert bids[0]['id'] == order['maker_order_id']
            if bids[0]['size'] == size:
                self.set_bids(price, bids[1:])
            else:
                bids[0]['size'] -= size
                self.set_bids(price, bids)
        else:
            asks = self.get_asks(price)
            if not asks:
                return
            assert asks[0]['id'] == order['maker_order_id']
            if asks[0]['size'] == size:
                self.set_asks(price, asks[1:])
            else:
                asks[0]['size'] -= size
                self.set_asks(price, asks)

    def change(self, order):
        try:
            new_size = float(order['new_size'])
            # new_size = order['new_size']
        except KeyError:
            return

        try:
            # price = order['price']
            price = float(order['price'])
        except KeyError:
            return

        if order['side'] == 'buy':
            bids = self.get_bids(price)
            if bids is None or not any(o['id'] == order['order_id'] for o in bids):
                return
            index = [b['id'] for b in bids].index(order['order_id'])
            bids[index]['size'] = new_size
            self.set_bids(price, bids)
        else:
            asks = self.get_asks(price)
            if asks is None or not any(o['id'] == order['order_id'] for o in asks):
                return
            index = [a['id'] for a in asks].index(order['order_id'])
            asks[index]['size'] = new_size
            self.set_asks(price, asks)

        tree = self._asks if order['side'] == 'sell' else self._bids
        node = tree.get(price)

        if node is None or not any(o['id'] == order['order_id'] for o in node):
            return
