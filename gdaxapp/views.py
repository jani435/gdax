# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time, sys
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader, Context
from orderbook_socket import OrderBookSocket
order_book = OrderBookSocket()

def index(request):
    if not order_book._started:
        try:
            order_book.start()
            order_book.set_started()
        except e:
            order_book.close()
    return render(request, "index.html")

def get_price(request):
    vals = {'mm_price': 'Calculating....', 'ticker': 'Calculating...'}
    if order_book.get_mm_price_db():
        vals['mm_price'] = order_book.get_mm_price_db()
    else:
        vals['mm_price'] = order_book.get_mm_price_api()
    if order_book.get_current_ticker():
        vals['ticker'] =  order_book.get_current_ticker()
    return render(request, "mmp.html", {'vals': vals})