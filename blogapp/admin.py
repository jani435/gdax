# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Post

# Register your models here.

class BlogAdmin(admin.ModelAdmin):
    exclude = ['create_date']
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Post, BlogAdmin)