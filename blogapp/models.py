# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import permalink
from django.utils import timezone

# Create your models here.
class Post(models.Model):
    
    title = models.CharField(max_length=200)
    post_text = models.TextField()
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    create_date = models.DateTimeField(default=timezone.now)
    pub_date = models.DateTimeField(blank=True, null=True) 
    slug = models.SlugField(max_length=100, unique=True)

    def __str__(self):
        return self.title

    def save_post(self):
        self.pub_date = timezone.now()
        self.save()

    @permalink
    def get_absolute_url(self):
        return ('view_blog_post', None, { 'slug': self.slug })