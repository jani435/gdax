from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.blog_posts, name='blog_posts'),
    url(r'^(?P<slug>[^\.]+).html', views.view_post, name='view_blog_post'),
]
