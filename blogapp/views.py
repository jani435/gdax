# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from models import Post
# Create your views here.
def blog_posts(request):
    posts = Post.objects.all().order_by('pub_date')
    return render(request, 'blog.html', {'posts': posts})

def view_post(request, slug):   
    post = get_object_or_404(Post, slug=slug)
    return render(request, 'post.html', {'post': post})
